﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound : MonoBehaviour
{
    public GameObject soundon, soundoff, effects;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("settings"))
        {
            effects.SetActive(false);
            soundon.SetActive(false);
            soundoff.SetActive(true);
        }
        if (Input.GetButtonDown("settings2"))
        {
            effects.SetActive(true);
            soundon.SetActive(true);
            soundoff.SetActive(false);
        }
    }
}
