﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotation_character : MonoBehaviour
{
    public Joystick joystickRotacion;
    // Update is called once per frame
    void Update()
    {
        float heading = Mathf.Atan2(joystickRotacion.Horizontal, joystickRotacion.Vertical);
        gameObject.transform.rotation = Quaternion.Euler(0f, 0f, - heading * Mathf.Rad2Deg);
    }
}
