﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class destroy_player : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            if (menu.time > 3.0f)
            {
                Destroy(this.gameObject);
                Destroy(collision.gameObject);
                SceneManager.LoadScene("death");
            }

        }
    }
    
}
