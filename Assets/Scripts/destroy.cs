﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;

public class destroy : MonoBehaviour
{
    public static float score = 10;
    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("funciona");
        if (collision.gameObject.tag == "kill")
        {
            Destroy(this.gameObject);
        }
        if(collision.gameObject.tag == "impact")
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            menu.score = menu.score + score;
        }
        if(collision.gameObject.tag == "enemy")
        {
            enter_boss.cont_enemy--;
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            menu.score = menu.score + score;
            
        }
    }

}
