﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speed : MonoBehaviour
{
    float speeds = 0.5f;
    public GameObject player;
    Vector2 position_player,position;
    // Start is called before the first frame update
    void Start()
    {
        position_player = player.transform.position;
        position = transform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2((position_player.x - position.x), (position_player.y - position.y)) * speeds * Time.deltaTime);
    }
}
