﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ebullet_speed : MonoBehaviour
{
    float speed = 0.75f;
    public GameObject player;
    Vector3 player_position, position;
    void Start()
    {
        player_position = player.transform.position;
        position = transform.position;
    }

    void Update()
    {
        
        transform.Translate((player_position-position) * speed * Time.deltaTime,Space.World);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "kill")
        {
            Destroy(this.gameObject);
        }
    }
}
