﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_enemy : MonoBehaviour
{
    float tiempoUltimoDisparo;
    public GameObject bullet;
    public AudioSource pium;

    void Start()
    {
    }

    void Update()
    {
        if (Time.time - tiempoUltimoDisparo > 3.5f)
        {
            tiempoUltimoDisparo = Time.time;
            GameObject new_bullet = Instantiate(bullet, transform.position, bullet.transform.rotation) as GameObject;
            new_bullet.SetActive(true);
            pium.Play();
        }
        pium.volume = menu.musicvol;
        
    }
}
