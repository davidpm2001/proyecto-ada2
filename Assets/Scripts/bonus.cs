﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bonus : MonoBehaviour
{
    public Text Tcountdown;
    public static float countdown = 3.0f;
    bool landscape = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        Tcountdown.text = (countdown).ToString("0");
        if(countdown <= 0)
        {
            if(landscape)
            {
                Screen.orientation = ScreenOrientation.LandscapeLeft;
            }
            else if (!landscape)
            {
                Screen.orientation = ScreenOrientation.LandscapeRight;
                landscape = true;
            }
            gameObject.SetActive(false);
            countdown = 3.0f;
        }
    }
}
