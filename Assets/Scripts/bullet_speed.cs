﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class bullet_speed : MonoBehaviour
{

    public float Speed = 3.0f;
    public Joystick joystickRotacion;
    public Vector2 direction;
    Vector2 direccionDisparo = Vector2.zero;

    void Update()
    {

        if (direccionDisparo == Vector2.zero)
        {
            direccionDisparo = new Vector2(joystickRotacion.Horizontal, joystickRotacion.Vertical);
            float heading = Mathf.Atan2(joystickRotacion.Horizontal, joystickRotacion.Vertical);
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, -heading * Mathf.Rad2Deg);
        }
        
        transform.Translate(direccionDisparo * Speed * Time.deltaTime, Space.World);
        
    }
}

