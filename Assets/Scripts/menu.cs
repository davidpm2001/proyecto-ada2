﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menu : MonoBehaviour
{
    bool isRunning = false;
    public GameObject panel, settings;
    public Text Ttime, Tscore, Tpowerup;
    public GameObject tpower;
    public static float score, timeleft = 10.0f, musicvol = 1.0f;
    int Starttime, stopTime, timertime;
    public static int time;
    Animator anim;
    void Start()
    {
        anim = settings.GetComponent<Animator>();
        TimerRestart();
        score = 0;
    }

    void Update()
    {
        if(character_controller2.power)
        {
            tpower.SetActive(true);
            timeleft -= Time.deltaTime;
            Tpowerup.text = (timeleft).ToString("0");
            
        }
        if (timeleft <= 0)
        {
            tpower.SetActive(false);
            character_controller2.power = false;
            timeleft = 10.0f;
        }


        StartTime();
        timertime = stopTime + ((int)Time.time - Starttime);
        time = timertime;
        Tscore.text = score.ToString();
        
        if(isRunning)
        {
            Ttime.text = timertime.ToString();
        }
    }
    public void Pause()
    {
        panel.SetActive(true);
        Time.timeScale = 0;
    }
    public void Resume()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
    }
    public void Menu()
    {
        timeleft = -1.0f;
        score = 0;
        TimerRestart();
        SceneManager.LoadScene("main_menu");
        Time.timeScale = 1;
        musicvol = 1.0f;
    }
    public void Play()
    {
        SceneManager.LoadScene("game");
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void Settings()
    {
        anim.SetBool("settings", true);
    }
    public void Back()
    {
        anim.SetBool("settings", false);
    }
    public void Retry()
    {
        timeleft = -1.0f;
        score = 0;
        TimerRestart();
        SceneManager.LoadScene("game");
        Time.timeScale = 1;
        musicvol = 1.0f;

    }
    public void StartTime()
    {
        if(!isRunning)
        {
            isRunning = true;
            Starttime = (int)Time.time;
        }
    }
    public void StopTime()
    {
        isRunning = false;
        stopTime = timertime;
    }
    public void TimerRestart()
    {
        stopTime = 0;
        isRunning = false;
    }
    public void SetVolume(float vol)
    {
        musicvol = vol;
    }
}
