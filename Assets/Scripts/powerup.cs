﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerup : MonoBehaviour
{
    public GameObject powerups;
    float diftiemp = 1;
    int random;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(Time.time - diftiemp > 1.0f)
        {
            diftiemp = Time.time;
            random = (int)Random.Range(1, 51);
            if (random == 25)
            {
                GameObject new_powerup = Instantiate(powerups, gameObject.transform.position, powerups.transform.rotation) as GameObject;
                new_powerup.SetActive(true);
            }
            
        }
        
    }
}
