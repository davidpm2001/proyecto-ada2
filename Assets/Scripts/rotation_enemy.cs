﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotation_enemy : MonoBehaviour
{
    public GameObject player;
    // Update is called once per frame
    void Update()
    {
        float heading = Mathf.Atan2(player.transform.position.x-transform.position.x, player.transform.position.y-transform.position.y);
        transform.rotation = Quaternion.Euler(0, 0, - heading * Mathf.Rad2Deg);
    }
}
