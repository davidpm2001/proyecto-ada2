﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character_controller2 : MonoBehaviour
{
    private Vector2 startingPoint, limitmathf;
    public Transform player;
    public GameObject bullet;
    public float speed = 15.0f, time, diftime;
    public float tiempoUltimoDisparo = 1.0f;
    Vector2 movpersonaje;
    public Joystick joystickRotacion;
    public Joystick joystickMovimiento;
    public static bool power = false;
    public AudioSource pium;


    void Start()
    {
        time = Time.time;
        limitmathf = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    void Update()
    {


        if (Mathf.Abs(joystickRotacion.Horizontal) > 0.001 && Mathf.Abs(joystickRotacion.Vertical) > 0.001)
        {
            float heading = Mathf.Atan2(joystickRotacion.Horizontal, joystickRotacion.Vertical);
            player.rotation = Quaternion.Euler(0f, 0f, heading * Mathf.Rad2Deg);

            if (Time.time-tiempoUltimoDisparo > 1.0f)
            {
                tiempoUltimoDisparo = Time.time;
                if (!power)
                {
                    GameObject new_bullet = Instantiate(bullet, player.transform.position, bullet.transform.rotation) as GameObject;
                    new_bullet.SetActive(true);
                }
                else
                {
                    GameObject new_bullet = Instantiate(bullet, player.transform.position, bullet.transform.rotation) as GameObject;
                    GameObject new_bullet2 = Instantiate(bullet, new Vector2(player.transform.position.x,player.transform.position.y +1.5f), bullet.transform.rotation) as GameObject;
                    GameObject new_bullet3 = Instantiate(bullet, new Vector2(player.transform.position.x, player.transform.position.y - 1.5f), bullet.transform.rotation) as GameObject;
                    new_bullet2.SetActive(true);
                    new_bullet3.SetActive(true);
                    new_bullet.SetActive(true);
                }
                pium.Play();
            }
        }
        if(Mathf.Abs(joystickMovimiento.Horizontal)>0.001 && Mathf.Abs(joystickMovimiento.Vertical)> 0.001)
        {
            movpersonaje = new Vector2(joystickMovimiento.Horizontal, joystickMovimiento.Vertical);
            player.transform.Translate(movpersonaje * speed * Time.deltaTime, Space.World);
        }


        pium.volume = menu.musicvol;


        Vector3 viewpos = player.transform.position;
        viewpos.x = Mathf.Clamp(viewpos.x, limitmathf.x * -1, limitmathf.x);
        viewpos.y = Mathf.Clamp(viewpos.y, limitmathf.y * -1, limitmathf.y);
        player.transform.position = viewpos;

        
    }


}
