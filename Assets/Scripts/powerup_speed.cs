﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerup_speed : MonoBehaviour
{
    float speed = 1.0f;
    public AudioSource wow;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        wow.volume = menu.musicvol;
        gameObject.transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "kill")
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "player")
        {
           character_controller2.power = true;
            wow.Play();
            Destroy(this.gameObject);
        }
    }

}
