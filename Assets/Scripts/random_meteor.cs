﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class random_meteor : MonoBehaviour
{
    private float random_positionx, random_positiony;
    private int random_time, random2x, random2y;
    public GameObject meteoro;
    bool seg;
    // Start is called before the first frame update
    void Start()
    {
        random_positiony = Random.Range(6, 7);
        random_positionx = Random.Range(10, 11);
        StartCoroutine(Meteor());   
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        random_time = Random.Range(1, 11);
        random2x = Random.Range(-1, 2);
        random2y = Random.Range(-1, 2);
        if(random2x == 1)
        {
            random_positionx = Random.Range(-10, 10);
            if(random2y == 1)
            {
                Debug.Log("6");
                random_positiony = Random.Range(7,8);
            }
            else if (random2y == 0)
            {
                Debug.Log("-6");
                random_positiony = Random.Range(-8,-7);
            }
        }
        else if(random2x == 0)
        {
            random_positiony = Random.Range(6, -6);
            if(random2y == 1)
            {
                Debug.Log("10");
                random_positionx = Random.Range(11, 12);
            }
            else if (random2y == 0)
            {
                Debug.Log("-10");
                random_positionx = Random.Range(-12, -11);
            }
        }
        if (menu.time > 60.0f)
        {
            seg = true;
        }


    }
    IEnumerator Meteor()
    {
        
        while (true)
        {
            if (!seg)
            {
                GameObject nuevo_meteoro = Instantiate(meteoro, meteoro.transform.position = new Vector2(random_positionx, random_positiony), meteoro.transform.rotation) as GameObject;
                nuevo_meteoro.SetActive(true);
            }
            yield return new WaitForSeconds(random_time);
        }
    }
    
}
