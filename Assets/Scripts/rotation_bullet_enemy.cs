﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotation_bullet_enemy : MonoBehaviour
{
    public GameObject player;
    float heading;
    void Start()
    {
        heading = Mathf.Atan2(player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.rotation = Quaternion.Euler(0, 0, -heading * Mathf.Rad2Deg);
    }
}
